import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  Component,
  OnChanges,
  SimpleChanges,
  Input,
  EventEmitter,
  Output,
  OnInit
} from '@angular/core';
import { Project } from 'src/app/shared/model/project.model';
import { ProjectAddModalComponent } from '../projects-info/project-add-modal/project-add-modal.component';
import { Card } from 'src/app/shared/model/card.model';

export class TabSts {
  public id: number;
  public name: string;
  public progres: number;
}

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit, OnChanges {
  @Input() project: Project;
  @Input() currentDate: Date;
  capacityavailable = 34;
  color = 'red';
  tabsts: TabSts[] = [];

  @Output() outputEvent: EventEmitter<Project> = new EventEmitter();
  @Output() inputDataChange = new EventEmitter();

  buttonStatus = false;
  icon = 'fas fa-cloud';

  chanegeButton() {
    console.log(this.buttonStatus);

    this.buttonStatus = !this.buttonStatus;
  }

  changeComponentValue(value) {
    console.log('changeComponentValue');
    console.log(value);
    this.project.nameProject = 'Ala ma kota';
    // this.project. nameProject = "Ala ma kota";
    this.outputEvent.emit(this.project); //<---this will identify element index you are dealing with
    // this.inputDataChange.emit(this.project); //<----new value will be assinged to an element that you are dealing with
  }

  constructor(private modalService: NgbModal) {}

  ngOnChanges(changes: SimpleChanges) {
    console.log('ngOnChanges, previous User: ', changes.project.previousValue);
    console.log('ngOnChanges, next User: ', changes.project.currentValue);
    console.log(
      'ngOnChanges, is first change?: ',
      changes.project.isFirstChange()
    );
      // dane do tablice
      this.getStsCard();

      //dane do zadania na dzis

      // dane do przeterminowane taski

  }
  ngOnInit() {

  }
  getStsCard() {
    this.project.card.forEach( (card: Card) => {
      let trueTask = 0;
      let falseTask = 0;

      card.tasks.forEach(task => {
        trueTask = ( task.status === true ) ?  trueTask + 1 : trueTask;
        falseTask = ( task.status === false ) ?  falseTask + 1 : falseTask;
      });

      const progress = ((trueTask + falseTask) !== 0) ? (trueTask / (trueTask + falseTask)) : 0;
      this.tabsts.push({id: card.id, name: card.name, progres: progress * 100} );
    });
  }

  openProject() {
    console.log('openFormModal');
    const modalRef = this.modalService.open(ProjectAddModalComponent);
    modalRef.componentInstance.project = this.project;
    modalRef.result
      .then(result => {
        console.log('Result');
        console.log(result);

        // this.projectService
        //   .addProject(JSON.stringify(result))
        //   .subscribe((result: Project) => {
        //     console.log("Dodanie nowego projektu");
        //     this.loadProject();
        //   });
      })
      .catch(error => {
        console.log('Error: ' + error);
      });
  }
}
