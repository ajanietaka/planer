import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ProjectComponent } from "./project/project.component";
import { AuthGuard } from "../helpers/auth.guard";
import { ProjectDetailComponent } from "./project-detail/project-detail.component";
import { ProjectsInfoComponent } from "./projects-info/projects-info.component";

const routes: Routes = [
  {
    path: "project",
    component: ProjectComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: "",
        component: ProjectsInfoComponent,
        canActivate: [AuthGuard]
      },
      {
        path: ":id",
        component: ProjectDetailComponent,
        canActivate: [AuthGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRoutingModule {}
