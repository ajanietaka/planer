import {
  Component,
  OnInit,
  ViewEncapsulation,
  ViewChild,
  ElementRef,
  Input,
  Output,
  EventEmitter
} from "@angular/core";

import "dhtmlx-gantt";
import { Router } from "@angular/router";
import { Project } from 'src/app/shared/model/project.model';
// import { ConsoleReporter } from 'jasmine';

declare let gantt: any;

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: "app-gantt",
  templateUrl: "./gantt.component.html",
  styleUrls: ["./gantt.component.css"]
  // providers: [TaskService, LinkService]
})
export class GanttComponent implements OnInit {

  @ViewChild("gantt_here", { static: true }) ganttContainer: ElementRef;

  tasks = [
    {
      id: 1,
      text: "Inzynierka",
      start_date: "2019-12-10",
      duration: 3,
      progress: 0.6
    },
    {
      id: 2,
      text: "Task #2",
      start_date: "2019-12-01",
      duration: 3,
      progress: 0.4
    }
  ];
  links = [{ id: 1, source: 1, target: 2, type: "0" }];

  @Input() project: Project;

  @Output() outputEvent: EventEmitter<Project> = new EventEmitter();
  @Output() inputDataChange = new EventEmitter();

  changeComponentValue(value) {
    console.log('changeComponentValue');
    console.log(value);
    this.project.nameProject = 'Ala ma kota';
    this.outputEvent.emit(this.project); //<---this will identify element index you are dealing with
    this.inputDataChange.emit(this.project); //<----new value will be assinged to an element that you are dealing with
    }

  constructor(private router: Router) {}



  ngOnInit() {
    console.log("ngOnInit - gantt")
   gantt.config.xml_date = '%Y-%m-%d %H:%i';
   gantt.config.layout = {
      css: "gantt_container",
      rows: [
        {
          cols: [
            {
              view: "grid",
              id: "grid",
              scrollX: "scrollHor",
              scrollY: "scrollVer"
            },
            { resizer: true, width: 1 },
            {
              view: "timeline",
              id: "timeline",
              scrollX: "scrollHor",
              scrollY: "scrollVer"
            },
            { view: "scrollbar", scroll: "y", id: "scrollVer" }
          ]
        },
        { view: "scrollbar", scroll: "x", id: "scrollHor", height: 20 }
      ]
    };
    gantt.init(this.ganttContainer.nativeElement);
    //   gantt.parse({
    //     data: [
    //         {id: 1, text: 'Project #2', start_date: '01-04-2013', duration: 18},
    //         {id: 2, text: 'Task #1',    start_date: '02-04-2013', duration: 8,
    //             progress: 0.6, parent: 1},
    //         {id: 3, text: 'Task #2',    start_date: '11-04-2013', duration: 8,
    //             progress: 0.6, parent: 1}
    //     ],
    //     links:[
    //         { id:1, source:1, target:2, type:1},
    //         { id:2, source:2, target:3, type:0}
    //   ]
    // });
    // tslint:disable-next-line:only-arrow-functions
   gantt.test = function(id) {
      alert(
        "You have clicked on the following task: " + gantt.getTask(id).text
      );
    };

    // gantt.attachEvent('onTaskClick', function(id, e ) {
    // // any custom logic here
    // console.log('open task');
    // // this.test(id); // Angular Function
    // // return true;
    // });

    // tslint:disable-next-line:only-arrow-functions
    // gantt.attachEvent('onTaskDrag', function(id, mode, task, original) {
    //   // any custom logic here
    //   console.log(id);
    //   console.log(mode);
    //   console.log(task);
    //   console.log(original);
    // });
    //     gantt.attachEvent("onBeforeTaskUpdate", function(id,new_item) {
    //     console.log('onBeforeTaskUpdate');
    //     console.log(id);
    //     console.log(new_item);
    //     console.log(this.data);

    // });

   gantt.attachEvent("onBeforeTaskAdd", function(id,item){
      console.log('onBeforeTaskAdd');
      console.log(id);
      console.log(item);
      return true;
  });

   gantt.attachEvent("onTaskCreated", function(task){
     console.log('onTaskCreated');
     console.log(task);
    task.projectId = 1;
    return true;
});
   gantt.attachEvent("onAfterTaskAdd", function(id,item){
    console.log('onAfterTaskAdd');
    console.log(id);
    console.log(item);
  //any custom logic here
});
    Promise.all([this.tasks, this.links]).then(([data, links]) => {
      gantt.parse({ data, links });
    });
  }
}
