import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  SimpleChanges,
  OnChanges
} from "@angular/core";
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem
} from "@angular/cdk/drag-drop";
import { Project } from "src/app/shared/model/project.model";
import { Card } from "src/app/shared/model/card.model";
import { Task } from "src/app/shared/model/task.model";
import { startOfDay } from "date-fns";
import { TaskService } from "src/app/shared/service/task.service";

@Component({
  selector: "app-kanban",
  templateUrl: "./kanban.component.html",
  styleUrls: ["./kanban.component.css"]
})
export class KanbanComponent implements OnInit, OnChanges {
  @Input() project: Project;

  @Output() outputEvent: EventEmitter<Project> = new EventEmitter();
  @Output() inputDataChange = new EventEmitter();

  cards: Card[] = [];
  connectedTo = [];

  constructor() {}

  ngOnInit() {}

  ngOnChanges(change: SimpleChanges) {
    console.log("Kanban-changes");
    console.log(this.project);

    this.cards = this.project.card;
    console.log(this.cards);
    for (const oneCard of this.cards) {
      this.connectedTo.push(oneCard.name);
    }
    console.log(this.connectedTo);
  }

  changeComponentValue(card, value) {
    console.log("changeComponentValue");
    console.log(value);
    this.project.nameProject = "Ala ma kota";
    this.outputEvent.emit(this.project); //<---this will identify element index you are dealing with
    this.inputDataChange.emit(
      new Task({
        id: 2,
        title: value,
        description: "Faza I",
        date: "2019-12-12",
        status: true,
        top3: false,
        category: null,
        position: 1
      })
    ); //<----new value will be assinged to an element that you are dealing with
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer.id === event.container.id) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
      // update data position
      this.cards.map((card: Card) => {
        if (card.name === event.container.id) {
          card.tasks.map((task: Task, index: number) => {
            task.position = index;
          });
        }
      });
      console.log("w pionie");
      console.log(this.cards);
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
      console.log("move");
      // update data position
      this.cards.map((card: Card) => {
        card.tasks.map((task: Task, index: number) => {
          task.position = index;
        });
      });
    }
  }

  dodaj(card: Card, value) {
    if (value) {
      console.log(value);
      console.log(card);
      // tslint:disable-next-line:max-line-length
      const addTaskItem = new Task({
        id: 2,
        title: value,
        description: "Faza ",
        date: "2019-12-09",
        status: true,
        top3: false,
        category: null,
        position: 1
      });
      // tslint:disable-next-line:no-shadowed-variable
      this.cards.map(cardIn => {
        if (cardIn.id === card.id) {
          cardIn.tasks.push(addTaskItem);
        }
      });
    }
  }
}
