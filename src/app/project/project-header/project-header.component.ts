import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-project-header',
  templateUrl: './project-header.component.html',
  styleUrls: ['./project-header.component.css']
})
export class ProjectHeaderComponent {

  @Input() viewProject: 'info' | 'gantt' | 'kanban';

  @Output() viewChange: EventEmitter<string> = new EventEmitter();

  constructor() { }

  viewChangeEmit(view: string) {
    this.viewChange.emit(view);
  }

}
