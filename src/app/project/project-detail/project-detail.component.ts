import {
  Component,
  OnInit,
  OnChanges,
  SimpleChange,
  Input,
  SimpleChanges,
  Output,
  EventEmitter
} from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { ProjectService } from "src/app/shared/service/project.service";
import { Project } from "src/app/shared/model/project.model";
import { Card } from "src/app/shared/model/card.model";
import { Task } from "src/app/shared/model/task.model";
import { TaskService } from "src/app/shared/service/task.service";
import { DateService } from "src/app/shared/service/date.service";

@Component({
  selector: "app-project-detail",
  templateUrl: "./project-detail.component.html",
  styleUrls: ["./project-detail.component.css"]
})
export class ProjectDetailComponent implements OnInit {
  viewProject = "";
  viewChange: any;
  currentDate: Date;

  project: Project;
  // @Output() projectChange: EventEmitter<Project> = new EventEmitter();
  // @Output() outputEvent: EventEmitter<string> = new EventEmitter();

  id$: Observable<string>;
  id: number;

  constructor(
    private route: ActivatedRoute,
    private projectService: ProjectService,
    private taskService: TaskService,
    private dateService: DateService
  ) {}

  ngOnInit() {
    // route parameters - route observables
    // this.id$ = this.route.paramMap.pipe(map(paramMap => paramMap.get('id')));
    // this.id$.subscribe(id => console.log(id));

    // route parameters - route snapshot
    this.id = Number(this.route.snapshot.paramMap.get("id"));
    console.log(this.id);

    // load project
    this.loadProject(this.id);
    this.dateService.currentDateTime.subscribe(
      date => (this.currentDate = date)
    );
    console.log(this.currentDate);
  }

  loadProject(id: number) {
    console.log("loadProject");
    this.projectService.getProjectById(id).subscribe((result: Project) => {
      // console.log("result- card");
      // console.log(result.card);
      // map cards
      const cardsTmp: Card[] = [];
      let oneCard: Card;
      result.card.map((card: Card) => {
        // tslint:disable-next-line:no-unused-expression
        if (card.tasks != null) {
          console.log("taski");
          const tasksTmp: Task[] = [];
          card.tasks.forEach(task => {
            tasksTmp.push(new Task(task));
          });
          card.tasks = tasksTmp;
        } else {
          console.log("nie ma zadan");
        }
        oneCard = new Card(card);
        cardsTmp.push(oneCard);
      });
      result.card = cardsTmp;
      this.project = new Project(result);
      //  console.log("result");
      //  console.log(result);
      //  console.log(this.project);
      this.setView("info");
    });
  }

  setView(view) {
    this.viewProject = view;
  }

  onComponentChange(value) {
    console.log("onComponentChange - project detail");
    console.log(value.tasks);
    this.project.nameProject = "Praca";

    // zmiany
  }
  onChangeTask(event) {
    console.log("onChangeData");
    console.log(event);
  }
}
