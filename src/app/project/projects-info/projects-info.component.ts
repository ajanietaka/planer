import {
  Component,
  OnInit,
  OnChanges,
  ViewChildren,
  QueryList
} from "@angular/core";
import { ProjectService } from "src/app/shared/service/project.service";
import { Project } from "src/app/shared/model/project.model";
import { Card } from "src/app/shared/model/card.model";
import { Router } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ProjectAddModalComponent } from "./project-add-modal/project-add-modal.component";
import { SortEvent, NgbdSortableHeader } from "./sortable.directive";
import { Status, statusList } from "../../shared/model/status.enum";

@Component({
  selector: "app-projects-info",
  templateUrl: "./projects-info.component.html",
  styleUrls: ["./projects-info.component.css"]
})
export class ProjectsInfoComponent implements OnInit {
  Status: String[] = [];
  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  projects: Project[] = [];
  projectsCache: Project[] = [];
  card: Card;

  constructor(
    private router: Router,
    private projectService: ProjectService,
    private modalService: NgbModal
  ) {
    this.filterForeCasts(event);
  }

  ngOnInit() {
    console.log("Get project from server");
    this.loadProject();
    // load status
    this.Status = statusList();
  }

  filterForeCasts(filterVal) {
    console.log(this.projects);
    console.log(this.projectsCache);
    if (filterVal == "0") this.projects = this.projectsCache;
    else
      this.projects = this.projectsCache.filter(
        item => item.statusProject == filterVal
      );
    console.log(this.projects);
  }
  onSort({ column, direction }: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = "";
      }
    });
    // sorting countries
    if (direction === "") {
      this.projects = this.projects;
    } else {
      this.projects = [...this.projects].sort((a, b) => {
        const res = this.compare(a[column], b[column]);
        return direction === "asc" ? res : -res;
      });
    }
    //this.sort(this.projects, column, direction);
    //this.projects.sortColumn = column;
    //this.service.sortDirection = direction;
  }

  compare(v1, v2) {
    return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
  }

  sort(projects: Project[], column: string, direction: string): Project[] {
    if (direction === "") {
      return projects;
    } else {
      return [...projects].sort((a, b) => {
        const res = this.compare(a[column], b[column]);
        return direction === "asc" ? res : -res;
      });
    }
  }

  loadProject() {
    console.log("Load data Project");
    this.projects = [];
    this.projectService.getProject().subscribe(
      (result: Project[]) => {
        result.forEach(element => {
          console.log(element);
          this.projects.push(element);
          this.projectsCache.push(element);
        });
      },
      error => {
        console.error("Error : " + Object.values(error));
      },
      () => {
        console.log("OK");
      }
    );
  }

  goToPage(project: Project) {
    console.log(project.id);
    this.router
      .navigate(["project/", project.id])
      .then(success => console.log("navigation success?", success))
      .catch(console.error);
  }

  addProject() {
    console.log("openFormModal");
    const modalRef = this.modalService.open(ProjectAddModalComponent);
    modalRef.result
      .then(result => {
        console.log("Result");
        console.log(result);
        this.projectService
          .addProject(JSON.stringify(result))
          .subscribe((result: Project) => {
            console.log("Dodanie nowego projektu");
            this.loadProject();
          });
      })
      .catch(error => {
        console.log("Error: " + error);
      });
  }
}
