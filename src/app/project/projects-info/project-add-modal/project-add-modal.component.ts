import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { concat } from 'rxjs';
import { statusList, Status } from 'src/app/shared/model/status.enum';
import { Project } from 'src/app/shared/model/project.model';

@Component({
  selector: 'app-project-add-modal',
  templateUrl: './project-add-modal.component.html',
  styleUrls: ['./project-add-modal.component.css']
})
export class ProjectAddModalComponent implements OnInit {
  @Input() project: Project = null;
  myForm: FormGroup;
  Status: String[] = [];
  defaultStatus = 'ACTIVE';
  statusProject: any;
  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.createForm();
    this.Status = statusList();
    // this.myForm.get("startProject").valueChanges.subscribe(value => {
    //   if (value) {
    //     this.myForm.get("endProject").setValue(null);
    //   } else {
    //     this.myForm.get("endProject").clearValidators();
    //   }
    // });
  }

  private createForm() {
    if (this.project === null){
      console.log('Pusta wartosc');
      this.myForm = this.formBuilder.group({
        nameProject: ['', Validators.required],
        descriptionProject: '',
        startProject: '',
        endProject: null,
        statusProject: [this.defaultStatus, Validators.required]
      });
    } else {
      console.log('Przekazana wartosc');
      console.log(this.project);
      this.myForm = this.formBuilder.group({
        nameProject: [this.project.nameProject, Validators.required],
        descriptionProject: this.project.descriptionProject,
        startProject: this.formatDate(this.project.startProject),
        endProject: this.project.endProject === null ? this.project.endProject : null,
        statusProject: [this.project.statusProject, Validators.required]
      });
    }
  }
  formatDate(In): NgbDateStruct {
    let dateIn = new Date(In.substring(0, 22));
    let date: NgbDateStruct = { year: dateIn.getFullYear(), month: dateIn.getMonth() + 1, day: dateIn.getDate() };
    console.log(dateIn);
    console.log(date);
    //this.myForm.controls['startProject'].setValue(date);
    // tslint:disable-next-line:no-unused-expression
    // date.year = 2019;
    // date.month = 12;
    // date.day = 12;
    return date;
  }

  changeStatus(e) {
    this.myForm.controls['statusProject'].setValue(e.target.value, {
      onlySelf: true
    });
  }
  public submitForm() {
    // const stsProj = this.myForm.controls["statusProject"].value;
    // this.myForm.controls["statusProject"].setValue(defaultStatus);
    if (this.myForm.controls['endProject'].value) {
      this.myForm.controls['endProject'].setValue(
        this.dateFormat(this.myForm.controls['endProject'].value)
      );
    }
    this.myForm.controls['startProject'].setValue(
      this.dateFormat(this.myForm.controls['startProject'].value)
    );
    this.activeModal.close(this.myForm.value);
  }
  private dateFormat(date: NgbDateStruct): any {
    const monthTmp = date.month + '';
    const dayTmp = date.day + '';
    let day = dayTmp.length < 2 ? '0' + date.day : date.day;
    let month = monthTmp.length < 2 ? '0' + date.month : date.month;
    return date.year + '-' + month + '-' + day;
  }
}
