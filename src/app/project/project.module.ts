import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ProjectRoutingModule } from "./project-routing.module";
import { ProjectComponent } from "./project/project.component";
import { ProjectDetailComponent } from "./project-detail/project-detail.component";
import { ProjectHeaderComponent } from "./project-header/project-header.component";
import { InfoComponent } from "./info/info.component";
import { KanbanComponent } from "./kanban/kanban.component";
import { GanttComponent } from "./gantt/gantt.component";

import { DragDropModule } from "@angular/cdk/drag-drop";
import { ProjectsInfoComponent } from "./projects-info/projects-info.component";
import { ProjectAddModalComponent } from "./projects-info/project-add-modal/project-add-modal.component";
import { NgbModal, NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgbdSortableHeader } from "./projects-info/sortable.directive";
import { ProjectCardComponent } from './project-modal/project-card/project-card.component';
import { ProjectTaskComponent } from './project-modal/project-task/project-task.component';

@NgModule({
  declarations: [
    ProjectComponent,
    ProjectDetailComponent,
    ProjectsInfoComponent,
    GanttComponent,
    KanbanComponent,
    InfoComponent,
    ProjectHeaderComponent,
    ProjectAddModalComponent,
    NgbdSortableHeader,
    ProjectCardComponent,
    ProjectTaskComponent
  ],
  imports: [
    CommonModule,
    ProjectRoutingModule,
    DragDropModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ],
  entryComponents: [ProjectAddModalComponent]
})
export class ProjectModule {}
