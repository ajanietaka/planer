import { Component, OnInit, Output} from '@angular/core';
import {formatDate} from '@angular/common';
import { DatePipe } from '@angular/common';
import { Days } from './shared/model/days.enum';
import { WeekService } from './shared/service/week.service';
import { Week } from './shared/model/week.model';
import { User } from './shared/model/user.model';
import { Router } from '@angular/router';
import { AuthenticationService } from './shared/service/authentication.service';
// tslint:disable-next-line:import-spacing


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @Output() currentDay: string;
  @Output() currentDateTime: Date;
  currentUser: User;
  

constructor(
    private router: Router,
    private authenticationService: AuthenticationService) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);


}


}
