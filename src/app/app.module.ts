import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgbModule, NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { NgbCollapseModule } from "@ng-bootstrap/ng-bootstrap";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { DatePipe } from "@angular/common";
import { TaskListComponent } from "./day/task/task-list/task-list.component";
import { TaskItemComponent } from "./day/task/task-item/task-item.component";
import { TaskComponent } from "./day/task/task.component";
import { NavbarComponent } from "./nav/navbar/navbar.component";
import { HomeComponent } from "./home/home/home.component";
import { DayComponent } from "./day/day/day.component";
import { TopComponent } from "./day/day/top/top.component";
import { HabitComponent } from "./day/day/habit/habit.component";
import { EatsComponent } from "./day/eats/eats.component";
import { WaterComponent } from "./day/water/water.component";
import { AlertComponent } from "./alert/alert.component";
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { JwtInterceptor } from "./helpers/jwt.interceptor";
import { ErrorInterceptor } from "./helpers/error.interceptor";
import { DateService } from "./shared/service/date.service";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { ScrollingModule } from "@angular/cdk/scrolling";
import { TaskModalComponent } from "./day/task-modal/task-modal.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { CalendarModule, DateAdapter } from "angular-calendar";
import { adapterFactory } from "angular-calendar/date-adapters/date-fns";
import { CalendarHeaderComponent } from "./calendar/calendar-header/calendar-header.component";
import { ProjectModule } from "./project/project.module";
import { NgbdTooltipBasic } from './shared/tooltip-basic';

@NgModule({
  declarations: [
    AppComponent,
    TaskListComponent,
    TaskItemComponent,
    TaskComponent,
    NavbarComponent,
    HomeComponent,
    DayComponent,
    TopComponent,
    HabitComponent,
    EatsComponent,
    WaterComponent,
    AlertComponent,
    LoginComponent,
    RegisterComponent,
    TaskModalComponent,
    CalendarHeaderComponent,
    NgbdTooltipBasic
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    NgbCollapseModule,
    HttpClientModule,
    DragDropModule,
    ScrollingModule,
    NgbModule,
    BrowserAnimationsModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    ProjectModule
  ],
  entryComponents: [TaskModalComponent],
  providers: [
    DatePipe,
    DateService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
