import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { User } from '../model/user.model';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class UserService {

    baseUrl = environment.baseUrl;
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(this.baseUrl + 'users');
    }

    register(user: User) {
        return this.http.post(this.baseUrl + 'registration', JSON.stringify(user),
        {headers: new HttpHeaders().set('Content-Type', 'application/json')
          });
    }

    delete(id: number) {
        return this.http.delete(this.baseUrl + `users/${id}`);
    }
}
