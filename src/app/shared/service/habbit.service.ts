import { Injectable } from '@angular/core';
import { Habbit } from '../model/habbit.model';
import { ItemHabbit } from '../model/itemHabbit.model';

@Injectable({
  providedIn: 'root'
})
export class HabbitService {

  public getHabbit(): Habbit[] {
    return [new Habbit(1, 'woda', 'picie wody', '2019-10-21', ' 2019-10-25', 'fas fa-coffee',
    [new ItemHabbit(1, '2019-10-25', false)])];
  }
  constructor() { }
}
