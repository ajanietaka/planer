import { Injectable } from '@angular/core';
import { DayService } from './day.service';
import { Week } from '../model/week.model';

@Injectable({
  providedIn: 'root'
})
export class WeekService {
  // public getWeek(id: number): Week {
  //   return new Week(id, [this.dayService.getDay()]);
  // }

  constructor(private dayService: DayService) { }
}
