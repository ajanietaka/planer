import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { AuthenticationService } from "./authentication.service";
import { ProjectDetailComponent } from "src/app/project/project-detail/project-detail.component";
import { Project } from "../model/project.model";

@Injectable({
  providedIn: "root"
})
export class ProjectService {
  baseUrl = environment.baseUrl;
  project: Project = null;

  constructor(
    private httpClient: HttpClient,
    private authenticationService: AuthenticationService
  ) {}

  getProject() {
    let url = this.baseUrl + "project";
    return this.httpClient.get(url);
  }
  addProject(json: string) {
    let url = this.baseUrl + "project";
    return this.httpClient.post(url, json, {
      headers: new HttpHeaders().set("Content-Type", "application/json")
    });
  }
  getProjectById<Project>(id: number) {
    let url = this.baseUrl + "project/" + id;
    return this.httpClient.get(url);
  }
  getCard(id: number) {
    let url = this.baseUrl + "card/" + id;
    return this.httpClient.get(url);
  }
}
