import { TestBed } from '@angular/core/testing';

import { HabbitService } from './habbit.service';

describe('HabbitService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HabbitService = TestBed.get(HabbitService);
    expect(service).toBeTruthy();
  });
});
