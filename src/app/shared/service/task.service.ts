import { Injectable } from "@angular/core";
import { Task } from "../model/task.model";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
  HttpParams
} from "@angular/common/http";
import { Observable, of } from "rxjs";
import { map } from "rxjs/operators/map";
import { AuthenticationService } from "./authentication.service";
import { environment } from 'src/environments/environment';

const headers = new HttpHeaders()
  .append("Content-Type", "application/json")
  .append("Access-Control-Allow-Headers", "Content-Type")
  .append("Access-Control-Allow-Methods", "GET, OPTIONS")
  .append("Access-Control-Allow-Origin", "http://localhost:8082");

// const endpoint = "http://localhost:8082/task";
const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/json"
  })
};

@Injectable({
  providedIn: "root"
})
export class TaskService {
  // public tasks: Task[];
  baseUrl = environment.baseUrl;
  url = "http://localhost:8082/user/tasks";

  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService
  ) {}

  getTask2(day: string): Observable<any> {
    let param = new HttpParams();
    param = param.append("day", day);
    return this.http.get<Task[]>(this.baseUrl + 'user/tasks', { params: param });
  }

  updateTask(task: Task): Observable<any> {
    return this.http.put(
      this.baseUrl + 'user/tasks',
      JSON.stringify(task),
      {
        headers: new HttpHeaders().set("Content-Type", "application/json")
      }
    );
  }
  addTaskPost(task: Task[]): Observable<any> {
    console.log("addTaskPost");
    console.log(JSON.stringify(task));
    return this.http.post(
      this.baseUrl + 'user/task',
      JSON.stringify(task),
      {
        headers: new HttpHeaders().set("Content-Type", "application/json")
      }
    );
  }

  //   public getTask(): Observable<Task> {
  //     return this.http.get('http://localhost:8082/task').pipe(
  //        map((data: any) => data.map((item: any) => {
  //           console.log('Jestem tutaj: ' + item.id);
  //           const task = new Task(
  //                     item.id,
  //                     item.nameTask,
  //                     item.descTask,
  //                     item.statusTask,
  //                     item.dateTask,
  //                     item.moveTaskToNextDay,
  //                   );
  //           console.log(task.toString());
  //           return task;
  //         }))
  //         );
  //  }

  //  public getProducts(): Observable<Task[]> {
  //   return this.http
  //     .get(this.url)
  //     .map(task => {
  //       return task.map((product) => new Task(product));
  //     })
  //     .catch((err) => console.error(err) );
  //   }

  // public getTask(): Observable<Task> {
  //  return this.http.get('http://localhost:8082/task')
  //  .pipe(map((response: any) => {
  //     return response.json.results.map(item => {
  //       console.log('Jestem tutaj: ' + item);
  //       return new Task(
  //         item.id,
  //         item.nameTask,
  //         item.descTask,
  //         item.statusTask,
  //         item.dateTask,
  //         item.moveTaskToNextDay
  //       );
  //     });
  //   }));
  // }
}
