import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DateService {

    private currentDate = new BehaviorSubject(new Date());
    currentDateTime = this.currentDate.asObservable();


    constructor() {}

   prevDay() {
   let tmp: Date;
   this.currentDateTime.subscribe(date => tmp = date);
   this.currentDate.next(new Date(tmp.setDate(tmp.getDate() - 1)));
  }
  nextDay() {
    let tmp: Date;
    this.currentDateTime.subscribe(date => tmp = date);
    this.currentDate.next(new Date(tmp.setDate(tmp.getDate() + 1)));
  }

}
