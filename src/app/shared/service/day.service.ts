import { Injectable } from '@angular/core';
import { TaskService } from './task.service';
import { HabbitService } from './habbit.service';
import { Day } from '../model/day.model';

@Injectable({
  providedIn: 'root'
})
export class DayService {

  // public getDay(): Day {
  //   return new Day(0, 'Poniedziałek', true, '2019-10-21', [this.taksService.getTask()], this.habbitService.getHabbit());
  // }

  constructor(private taksService: TaskService, private habbitService: HabbitService) { }
}
