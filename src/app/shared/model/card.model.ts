import { Task } from "./task.model";

export class Card {
  // public createdAt: Date;
  // public updatedAt: Date;
  public id: number;
  public name: string;
  public start: Date;
  public end: Date;
  public position: number;
  public tasks: Task[];

  constructor(obj?: any) {
    Object.assign(this, obj);
  }
}
