export class ItemHabbit {
    public id: number;
    public dateHabbit: string;
    public statusHasbbit: boolean;

    constructor(id: number, dateHabbit: string, statusHabbit: boolean) {
        this.id = id;
        this.dateHabbit = dateHabbit;
        this.statusHasbbit = statusHabbit;
    }
}
