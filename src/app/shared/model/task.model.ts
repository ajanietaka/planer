import { Card } from "./card.model";

export class Task {
  public id: number;
  public title: string;
  public description: string;
  public startDate: Date;
  public endDate: Date;
  public allDay: boolean;
  public status: boolean;
  public top: boolean;
  public category: string;
  public position: number;

  constructor(obj?: any) {
    Object.assign(this, obj);
  }

  // tslint:disable-next-line:max-line-length
  // constructor(id: number, nameTask: string, descriptionTask: string, statusTask: boolean, dateTask: string, moveTaskToNextDay: boolean, category: string ) {
  //     this.id = id;
  //     this.title = nameTask;
  //     this.description = descriptionTask;
  //     this.status = statusTask;
  //     this.date = dateTask;
  //     this.top3 = moveTaskToNextDay;
  //     this.category = category;
  // }

  toString() {
    // tslint:disable-next-line:max-line-length
    return (
      this.id +
      " - " +
      this.title +
      " " +
      this.description +
      " " +
      this.status +
      " " +
      this.startDate +
      " " +
      this.category
    );
  }
}
