import { Day } from './day.model';

export class Week {

    public id: number;
    public days: Day[];

    constructor(id: number, days: Day[]) {
      this.id = id;
      this.days = days;
    }
}
