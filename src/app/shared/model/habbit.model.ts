import { ItemHabbit } from './itemHabbit.model';

export class Habbit {
    public id: number;
    public nameHabbit: string;
    public descHabbit: string;
    public startDate: string;
    public endDate: string;
    public iconHabbit: string;
    public habbits: ItemHabbit[];

    // tslint:disable-next-line:max-line-length
    constructor(id: number, nameHabbit: string, descHabbit: string, startDate: string, endDate: string, iconHabbit: string, habbits: ItemHabbit[]) {
        this.id = id;
        this.nameHabbit = nameHabbit;
        this.descHabbit = descHabbit;
        this.startDate = startDate;
        this.endDate = endDate;
        this.iconHabbit = iconHabbit;
        this.habbits = habbits;
    }
}
