import { NgControlStatusGroup } from '@angular/forms';

export enum Status {
  "ACTIVE",
  "PLANNED",
  "SUSPENDED",
  "CLOSED",
  "DELETED"
}
export function statusList() {
  const statusList = [];
  for (let sts in Status) {
    if (typeof Status[sts] === "number") {
      statusList.push(sts);
    }
  }
  return statusList;
}

