import { Card } from './card.model';

export class Project {
    public id: number;
    public nameProject: string;
    public descriptionProject: string;
    public startProject: Date;
    public endProject: Date;
    public statusProject: boolean;
    public card: Card[];

    constructor(obj?: any) {
        Object.assign(this, obj);
    }
}
