import { Task } from './task.model';
import { Habbit } from './habbit.model';
import { Injectable } from '@angular/core';
import { TaskService } from '../service/task.service';
import { HabbitService } from '../service/habbit.service';

@Injectable()
export class Day {
 public id: number;
 public dayName: string;
 public currentDay: boolean;
 public dayDate: string;
 public tasks: Task[];
 public habbits: Habbit[];

 constructor(id: number, dayName: string, currentDay: boolean, dayDate: string, tasks: Task[], habbits: Habbit[] ) {
   this.id = id;
   this.dayName = dayName;
   this.currentDay = true;
   this.dayDate = dayDate;
   this.tasks = tasks;
   this.habbits = habbits;
 }
}
