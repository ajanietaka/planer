// import { NgModule, Component } from '@angular/core';
// import { Routes, RouterModule } from '@angular/router';
// import { HomeComponent } from './home/home/home.component';
// import { DayComponent } from './day/day/day.component';

// const routes: Routes = [
//   { path: '', component: HomeComponent },
//   { path: 'day', component: DayComponent }
// ];

// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule]
// })
// export class AppRoutingModule { }
import { Routes, RouterModule } from "@angular/router";

import { HomeComponent } from "./home/home/home.component";
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { AuthGuard } from "./helpers/auth.guard";
import { NgModule } from "@angular/core";
import { DayComponent } from "./day/day/day.component";
import { ProjectModule } from "./project/project.module";

const routes: Routes = [
  { path: "home", pathMatch: 'full', component: HomeComponent, canActivate: [AuthGuard] },
  { path: "login", component: LoginComponent },
  { path: "register", component: RegisterComponent },
  // { path: 'calendar', component: CalendarCo, canActivate: [AuthGuard]},
  { path: "planer", component: DayComponent, canActivate: [AuthGuard] },

  // otherwise redirect to home
  { path: "**", redirectTo: "home" }
];

@NgModule({
  imports: [ProjectModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
