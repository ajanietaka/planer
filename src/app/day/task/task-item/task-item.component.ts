import { Component, OnInit, Input } from '@angular/core';
import { Task } from 'src/app/shared/model/task.model';

@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.css']
})
export class TaskItemComponent implements OnInit {
  @Input() task: Task;

  constructor() { }

  ngOnInit() {
    console.log(this.task.status);
  }

  changedStatus(task) {
    this.task = task;
    console.log(task.id + ' ' + task.status);
  }
}
