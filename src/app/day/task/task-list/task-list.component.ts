import { Component, OnInit, OnChanges } from '@angular/core';
import { Task } from '../../../shared/model/task.model';
import { TaskService } from 'src/app/shared/service/task.service';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit, OnChanges {
  public tasks: Task[];

  constructor(private taskService: TaskService) { }

  ngOnInit() {
   // this.tasks = this.taskService.getTasks();
  }

  ngOnChanges() {
    // console.log('ngOnChanges');
    // this.tasks.forEach( (element: Task) => {
    //   console.log(element.toString());
    // });
  }

}
