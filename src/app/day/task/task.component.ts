import {
  Component,
  OnInit,
  OnChanges,
  Input,
  SimpleChange,
  SimpleChanges
} from "@angular/core";
import { TaskService } from "src/app/shared/service/task.service";
import { Task } from "src/app/shared/model/task.model";
import { Observable } from "rxjs";
import { map, catchError, tap, reduce } from "rxjs/operators";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { TaskModalComponent } from "../task-modal/task-modal.component";
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem
} from "@angular/cdk/drag-drop";

@Component({
  selector: "app-task",
  templateUrl: "./task.component.html",
  styleUrls: ["./task.component.css"]
})
export class TaskComponent implements OnInit, OnChanges {
  @Input() currentDay: string;
  tasks: Task[] = [];

  preventSingleClick = false;
  timer: any;
  delay: number;

  constructor(
    private modalService: NgbModal,
    private taskService: TaskService
  ) {}

  ngOnInit() {
    // this.loadTask(this.currentDay);
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log("ngOnChanges");
    console.log(changes);
    this.loadTask(this.currentDay);
  }

  loadTask(date: string) {
    console.log("Load data");
    this.tasks = [];
    this.taskService.getTask2(this.currentDay).subscribe(
      (result: Task[]) => {
        result.forEach(element => {
          this.tasks.push(element);
        });
        this.tasks = this.tasks.filter((task: Task) => task.top === false);
        console.log(this.tasks);
      },
      error => {
        console.error("Error : " + Object.values(error));
      },
      () => {
        console.log("Task1");
      }
    );
  }

  changedStatus(task: Task) {
    console.log("changedTask");
    task.status = !task.status;
    const listTask = [];
    listTask.push(task);
    this.taskService.updateTask(task).subscribe((result: Task) => {
      console.log(result);
    });
    //this.loadTask(this.currentDay);
  }
  addTask(newTask: string) {
    if (newTask) {
      console.log("newTask: " + newTask);
      // tslint:disable-next-line:max-line-length
      const addTaskItem = new Task({
        createdAt: null,
        updatedAt: null,
        title: newTask,
        description: "obiad",
        date: this.currentDay,
        status: true,
        top3: false,
        position: this.tasks.length
      });
      // this.countries.push({ id: this.countries.length + 1, name: newTask, status: false });
      this.tasks.push(addTaskItem);
      const sendTask = [];
      sendTask.push(addTaskItem);
      // send
      this.taskService.addTaskPost(sendTask).subscribe((result: Task) => {
        console.log("addTaskPost");
        console.log(result);
      });
    }
  }
  okay(): void {
    console.log(this.tasks);
    console.log(this.tasks.length);
    for (const i of this.tasks) {
      console.log("tutaj: " + i);
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
      const i = 0;
      // tslint:disable-next-line:no-shadowed-variable
      this.tasks.map((k, i) => {
        k.position = i;
      });
      // TODO update db, send all task in one request
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
  }
  singleClick(event) {
    this.preventSingleClick = false;
    const delay = 200;
    this.timer = setTimeout(() => {
      if (!this.preventSingleClick) {
        console.log("Single Click Event");
      }
    }, delay);
  }

  doubleClick(item) {
    this.preventSingleClick = true;
    clearTimeout(this.timer);
    this.changedStatus(item);
  }

  openFormModal(item: Task) {
    console.log("openFormModal");
    const modalRef = this.modalService.open(TaskModalComponent);
    modalRef.componentInstance.item = item;
    modalRef.result
      .then(result => {
        console.log("Result");
        if (JSON.stringify(item) === JSON.stringify(result)) {
          console.log("Takie same");
        } else {
          console.log("Różne");
          // send task do db update
        }
      })
      .catch(error => {
        console.log("Error: " + error);
      });
  }
}
