import { Component, OnInit, Input, Directive } from "@angular/core";
import { NgbActiveModal, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { FormGroup, FormBuilder, NgControl } from "@angular/forms";
import { Task } from "src/app/shared/model/task.model";

// @Directive({
//   // tslint:disable-next-line:directive-selector
//   selector: '[disableControl]'
// })
// export class DisableControlDirective {
//   @Input() set disableControl(condition: boolean) {
//     const action = condition ? "disable" : "enable";
//     this.ngControl.control[action]();
//   }

//   constructor(private ngControl: NgControl) {}
// }

@Component({
  selector: "app-task-modal",
  templateUrl: "./task-modal.component.html",
  styleUrls: ["./task-modal.component.css"]
})
export class TaskModalComponent implements OnInit {
  @Input() public id: number;
  @Input() public item: Task;
  @Input() public myModalTitle = "Tak";
  myForm: FormGroup;
  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    console.log("ngOnInit modal");
    console.log(this.item);
    this.createForm();
  }

  private createForm() {
    this.myForm = this.formBuilder.group({
      id: this.item.id,
      title: this.item.title,
      description: this.item.description,
      date: this.item.startDate,
      status: this.item.status,
      top3: this.item.top,
      category: this.item.category
    });
  }
  public submitForm() {
    this.activeModal.close(this.myForm.value);
  }
}
