import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  Output
} from '@angular/core';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem
} from '@angular/cdk/drag-drop';
import { identifierModuleUrl } from '@angular/compiler';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TaskModalComponent } from '../../task-modal/task-modal.component';
import { Task } from 'src/app/shared/model/task.model';
import { TaskService } from 'src/app/shared/service/task.service';
import { DateService } from 'src/app/shared/service/date.service';

@Component({
  selector: 'app-top',
  templateUrl: './top.component.html',
  styleUrls: ['./top.component.css']
})
export class TopComponent implements OnInit, OnChanges {
  tasksTop: Task[] = [];
  @Input() currentDay: string;

  sizeTop3 = 2;
  sizeTodos = 0;
  lessons = false;

  // export class Task {

  preventSingleClick = false;
  timer: any;
  delay: number;

  constructor(
    private modalService: NgbModal,
    private taskService: TaskService
  ) {}

  ngOnInit() {
    console.log('Top component');
    // console.log(this.currentDay);
    // this.tasksTop = [
    //   { createdAt: null, updatedAt: null, id: 1, title: 'Ugotowac obiad', description: 'obiad', date: '2019-10-15', status: true, top3: false, category: null, position: 0 },
    //   { createdAt: null, updatedAt: null, id: 2, title: 'Ugotowac kolacje', description: null, date: '2019-10-15', status: true, top3: false, category: null, position: 1 }

    // ];
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('Top compoment');
    console.log(changes);
    this.loadTask(this.currentDay);
  }

  loadTask(day: string) {
    this.tasksTop = [];
    this.taskService.getTask2(day).subscribe(
      (result: Task[]) => {
        result.forEach(element => {
          this.tasksTop.push(new Task(element));
        });
        this.tasksTop = this.tasksTop.filter((task: Task) => task.top === true);
      },
      error => {
        console.error('Error : ' + Object.values(error));
      },
      () => {
        console.log('Taski pobrane');
      }
    );
  }

  changedStatus(item: any) {
    item.status = !item.status;
  }
  addTask(newTask: string) {
    if (newTask) {
      console.log('newTask: ' + newTask);
      // tslint:disable-next-line:max-line-length
      const addTaskItem = new Task({
        createdAt: null,
        updatedAt: null,
        title: newTask,
        description: null,
        date: this.currentDay,
        status: false,
        top3: true,
        category: null,
        position: this.tasksTop.length
      });
      // this.countries.push({ id: this.countries.length + 1, name: newTask, status: false });
      this.tasksTop.push(addTaskItem);
      const sendTask = [];
      sendTask.push(addTaskItem);
      // send
      this.taskService.addTaskPost(sendTask).subscribe((result: Task) => {
        console.log('addTaskPost');
        console.log(result);
      });
    }
  }
  okay(): void {
    console.log(this.tasksTop);
    console.log(this.tasksTop.length);
    for (const i of this.tasksTop) {
      console.log('tutaj: ' + i);
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
      const i = 0;
      this.tasksTop.map((k, i) => {
        k.position = i;
      });
      // TODO update db, send all task in one request
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
  }
  singleClick(event) {
    this.preventSingleClick = false;
    const delay = 200;
    this.timer = setTimeout(() => {
      if (!this.preventSingleClick) {
        console.log('Single Click Event');
      }
    }, delay);
  }

  doubleClick(item) {
    this.preventSingleClick = true;
    clearTimeout(this.timer);
    this.changedStatus(item);
  }

  openFormModal(item: Task) {
    console.log('openFormModal');
    const modalRef = this.modalService.open(TaskModalComponent);
    modalRef.componentInstance.item = item;
    modalRef.result
      .then(result => {
        console.log('Result');
        if (JSON.stringify(item) === JSON.stringify(result)) {
          console.log('Takie same');
        } else {
          console.log('Różne');
          // send task do db update
        }
      })
      .catch(error => {
        console.log('Error: ' + error);
      });
  }
}
