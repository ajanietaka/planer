import { Component, OnInit, Input, Output } from '@angular/core';
import { DatePipe } from '@angular/common';
import { DateService } from 'src/app/shared/service/date.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TaskModalComponent } from '../task-modal/task-modal.component';
import { Task } from 'src/app/shared/model/task.model';
import { TaskService } from 'src/app/shared/service/task.service';

@Component({
  selector: 'app-day',
  templateUrl: './day.component.html',
  styleUrls: ['./day.component.css']
})
export class DayComponent implements OnInit {
  public dzien = 'Poniedziałek';
  @Output() tasksTop: Task[] = [];
  currentDay: string;
  currentDateTime: Date;

  constructor(private dateService: DateService,
              private datePipe: DatePipe,
              private modalService: NgbModal) { }

  ngOnInit() {
    this.dateService.currentDateTime.subscribe(date => this.currentDateTime = date);
    console.log('DAY COMP currentDay: ' + this.currentDateTime);
    this.currentDay = this.datePipe.transform(this.currentDateTime, 'yyyy-MM-dd');
  }
  prevDay() {
    this.dateService.prevDay();
    this.currentDay = this.datePipe.transform(this.currentDateTime, 'yyyy-MM-dd');
  }
  nextDay() {
    this.dateService.nextDay();
    this.currentDay = this.datePipe.transform(this.currentDateTime, 'yyyy-MM-dd');
  }

  open() {
    const modalRef = this.modalService.open(TaskModalComponent);
    modalRef.componentInstance.myModalTitle = 'I your title';
    modalRef.componentInstance.my_modal_content = 'I am your content';
  }

}

